package model;

import java.io.Serializable;
import java.util.UUID;

public class NonNormalizedMsg implements Serializable {
    private UUID userId;
    private UUID messageId;
    private UUID forumId;
    private UUID themeId;
    private UUID roleId;
    private String username;
    private String email;
    private String body;
    private String subject;
    private String name;
    private Boolean canEdit;
    private Boolean canDelete;
    private Boolean canPost;
    private String font;
    private String language;
    private String color;

    public NonNormalizedMsg() {
    }

    public NonNormalizedMsg(UUID userId, UUID messageId, UUID forumId, UUID themeId, UUID roleId, String username, String email, String body, String subject, String name, Boolean canEdit, Boolean canDelete, Boolean canPost, String font, String language, String color) {
        this.userId = userId;
        this.messageId = messageId;
        this.forumId = forumId;
        this.themeId = themeId;
        this.roleId = roleId;
        this.username = username;
        this.email = email;
        this.body = body;
        this.subject = subject;
        this.name = name;
        this.canEdit = canEdit;
        this.canDelete = canDelete;
        this.canPost = canPost;
        this.font = font;
        this.language = language;
        this.color = color;
    }

    @Override
    public String toString() {
        return "NonNormalizedMsg{" +
                "userId=" + userId +
                ", messageId=" + messageId +
                ", forumId=" + forumId +
                ", themeId=" + themeId +
                ", roleId=" + roleId +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", body='" + body + '\'' +
                ", subject='" + subject + '\'' +
                ", name='" + name + '\'' +
                ", canEdit=" + canEdit +
                ", canDelete=" + canDelete +
                ", canPost=" + canPost +
                ", font='" + font + '\'' +
                ", language='" + language + '\'' +
                ", color='" + color + '\'' +
                '}';
    }

    public UUID getUserId() {
        return userId;
    }

    public void setUserId(UUID userId) {
        this.userId = userId;
    }

    public UUID getMessageId() {
        return messageId;
    }

    public void setMessageId(UUID messageId) {
        this.messageId = messageId;
    }

    public UUID getForumId() {
        return forumId;
    }

    public void setForumId(UUID forumId) {
        this.forumId = forumId;
    }

    public UUID getThemeId() {
        return themeId;
    }

    public void setThemeId(UUID themeId) {
        this.themeId = themeId;
    }

    public UUID getRoleId() {
        return roleId;
    }

    public void setRoleId(UUID roleId) {
        this.roleId = roleId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getCanEdit() {
        return canEdit;
    }

    public void setCanEdit(Boolean canEdit) {
        this.canEdit = canEdit;
    }

    public Boolean getCanDelete() {
        return canDelete;
    }

    public void setCanDelete(Boolean canDelete) {
        this.canDelete = canDelete;
    }

    public Boolean getCanPost() {
        return canPost;
    }

    public void setCanPost(Boolean canPost) {
        this.canPost = canPost;
    }

    public String getFont() {
        return font;
    }

    public void setFont(String font) {
        this.font = font;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
