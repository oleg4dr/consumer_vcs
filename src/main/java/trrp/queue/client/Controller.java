package trrp.queue.client;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/mqservice")
public class Controller {
    private final
    ServiceImplementation serviceImplementation;

    public Controller(ServiceImplementation serviceImplementation) {
        this.serviceImplementation = serviceImplementation;
    }

    //обновить ключи
    @PostMapping(value = "/refreshkeys")
    public String refreshKeys(){
        serviceImplementation.generateAndSendPublicKey();
        return "public key was sent to application, waiting for encrypted msg";
    }
}
