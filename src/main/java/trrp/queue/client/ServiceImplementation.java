package trrp.queue.client;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;
import util.GenerateKeys;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import org.springframework.amqp.core.Queue;

@Service
public class ServiceImplementation {

    private Queue  publicKeyQueue = new Queue("publickey");

    private final RabbitTemplate template;

    public ServiceImplementation(RabbitTemplate template) {
        this.template = template;
    }

    //генерация публичного ключа и отправка его приложению
    void generateAndSendPublicKey(){
        try {
            GenerateKeys gk = new GenerateKeys(1024);
            //создаем связку
            gk.createKeys();
            // сохраняем пару ключей в файлы, нам они еще потребуются для дешифровки
            gk.writeToFile("KeyPair/publicKey", gk.getPublicKey().getEncoded());
            gk.writeToFile("KeyPair/privateKey", gk.getPrivateKey().getEncoded());
            //отправим ключ в очередь которую слушает приложение
            this.template.convertAndSend(publicKeyQueue.getName(), gk.getPublicKey());
        } catch (NoSuchAlgorithmException | NoSuchProviderException | IOException e) {
            e.printStackTrace();
        }
    }

}
