package trrp.queue.client;

import model.NonNormalizedMsg;
import org.jooq.DSLContext;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import util.AsymmetricCryptography;
import util.MyConverter;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import static database.Tables.*;

@Component
public class Consumer {

    private final
    DSLContext create;

    public Consumer(DSLContext create) {
        this.create = create;
    }

    //слушатель новых зашифрованных ключей симмметричного шифрования
    @RabbitListener(queues = "AESKey")
    public void receiveRSAKey(byte[] receivedKey) throws Exception {
        //сначала расшифруем сообщение зашифрованное асиметричным публичным ключем с помощью сгенерированного ранее приватного
        AsymmetricCryptography ac = new AsymmetricCryptography();
        byte[] DecryptedAESKey = ac.decryptMsg(receivedKey, ac.getPrivate("KeyPair/privateKey"));
        //сохранить ключ в файл чтобы было сохранение между сессиями
        ac.writeToFile(new File("KeyPair/AESKey"),DecryptedAESKey);
        System.out.println("Получен новый симметричный ключ и расшифрован с помощью ассимитричного приватного");
    }

    @RabbitListener(queues = "RSA")
    public void receiveRSAMsg(byte[] receivedData) throws NoSuchPaddingException, NoSuchAlgorithmException, IOException, BadPaddingException, IllegalBlockSizeException, InvalidKeyException, ClassNotFoundException {
       //достать ключ из файла
        byte[] AESKey = Files.readAllBytes(new File("KeyPair/AESKey").toPath());
        decryptAESMsg(receivedData, AESKey);
    }

    private void decryptAESMsg(byte[] receivedData, byte[] decryptedAESKey) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IOException, ClassNotFoundException, IllegalBlockSizeException, BadPaddingException {
        System.out.println("Encrypted data: " + Arrays.toString(receivedData));
        Cipher c =Cipher.getInstance("AES");
        SecretKeySpec k = new SecretKeySpec(decryptedAESKey, "AES");
        c.init(Cipher.DECRYPT_MODE, k);
        NonNormalizedMsg msg = (NonNormalizedMsg) MyConverter.deserialize(c.doFinal(receivedData));
        System.out.println("Decrypted data: " + msg);
        insertToDB(msg);
    }

    @Transactional
    void insertToDB(NonNormalizedMsg msg) {
        create.insertInto(FORUMS).values(msg.getForumId(), msg.getName()).execute();
        create.insertInto(ROLES).values(msg.getCanEdit(), msg.getRoleId(), msg.getCanDelete(), msg.getCanPost()).execute();
        create.insertInto(THEMES).values(msg.getThemeId(), msg.getFont(), msg.getLanguage(), msg.getColor()).execute();
        create.insertInto(USERS).values(msg.getUserId(), msg.getUsername(), msg.getEmail(), msg.getThemeId(), msg.getRoleId()).execute();
        create.insertInto(MESSAGES).values(msg.getMessageId(), msg.getUserId(), msg.getForumId(), msg.getBody(), msg.getSubject()).execute();
        System.out.println("Converted non normalized data to normalized and saved in db");
    }
}
