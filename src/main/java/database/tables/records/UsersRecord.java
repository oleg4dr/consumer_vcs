/*
 * This file is generated by jOOQ.
 */
package database.tables.records;


import database.tables.Users;

import java.util.UUID;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record5;
import org.jooq.Row5;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.3"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class UsersRecord extends UpdatableRecordImpl<UsersRecord> implements Record5<UUID, String, String, UUID, UUID> {

    private static final long serialVersionUID = 1726897481;

    /**
     * Setter for <code>Clients.users.userId</code>.
     */
    public void setUserid(UUID value) {
        set(0, value);
    }

    /**
     * Getter for <code>Clients.users.userId</code>.
     */
    public UUID getUserid() {
        return (UUID) get(0);
    }

    /**
     * Setter for <code>Clients.users.username</code>.
     */
    public void setUsername(String value) {
        set(1, value);
    }

    /**
     * Getter for <code>Clients.users.username</code>.
     */
    public String getUsername() {
        return (String) get(1);
    }

    /**
     * Setter for <code>Clients.users.email</code>.
     */
    public void setEmail(String value) {
        set(2, value);
    }

    /**
     * Getter for <code>Clients.users.email</code>.
     */
    public String getEmail() {
        return (String) get(2);
    }

    /**
     * Setter for <code>Clients.users.themeId</code>.
     */
    public void setThemeid(UUID value) {
        set(3, value);
    }

    /**
     * Getter for <code>Clients.users.themeId</code>.
     */
    public UUID getThemeid() {
        return (UUID) get(3);
    }

    /**
     * Setter for <code>Clients.users.roleId</code>.
     */
    public void setRoleid(UUID value) {
        set(4, value);
    }

    /**
     * Getter for <code>Clients.users.roleId</code>.
     */
    public UUID getRoleid() {
        return (UUID) get(4);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    @Override
    public Record1<UUID> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record5 type implementation
    // -------------------------------------------------------------------------

    @Override
    public Row5<UUID, String, String, UUID, UUID> fieldsRow() {
        return (Row5) super.fieldsRow();
    }

    @Override
    public Row5<UUID, String, String, UUID, UUID> valuesRow() {
        return (Row5) super.valuesRow();
    }

    @Override
    public Field<UUID> field1() {
        return Users.USERS.USERID;
    }

    @Override
    public Field<String> field2() {
        return Users.USERS.USERNAME;
    }

    @Override
    public Field<String> field3() {
        return Users.USERS.EMAIL;
    }

    @Override
    public Field<UUID> field4() {
        return Users.USERS.THEMEID;
    }

    @Override
    public Field<UUID> field5() {
        return Users.USERS.ROLEID;
    }

    @Override
    public UUID component1() {
        return getUserid();
    }

    @Override
    public String component2() {
        return getUsername();
    }

    @Override
    public String component3() {
        return getEmail();
    }

    @Override
    public UUID component4() {
        return getThemeid();
    }

    @Override
    public UUID component5() {
        return getRoleid();
    }

    @Override
    public UUID value1() {
        return getUserid();
    }

    @Override
    public String value2() {
        return getUsername();
    }

    @Override
    public String value3() {
        return getEmail();
    }

    @Override
    public UUID value4() {
        return getThemeid();
    }

    @Override
    public UUID value5() {
        return getRoleid();
    }

    @Override
    public UsersRecord value1(UUID value) {
        setUserid(value);
        return this;
    }

    @Override
    public UsersRecord value2(String value) {
        setUsername(value);
        return this;
    }

    @Override
    public UsersRecord value3(String value) {
        setEmail(value);
        return this;
    }

    @Override
    public UsersRecord value4(UUID value) {
        setThemeid(value);
        return this;
    }

    @Override
    public UsersRecord value5(UUID value) {
        setRoleid(value);
        return this;
    }

    @Override
    public UsersRecord values(UUID value1, String value2, String value3, UUID value4, UUID value5) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached UsersRecord
     */
    public UsersRecord() {
        super(Users.USERS);
    }

    /**
     * Create a detached, initialised UsersRecord
     */
    public UsersRecord(UUID userid, String username, String email, UUID themeid, UUID roleid) {
        super(Users.USERS);

        set(0, userid);
        set(1, username);
        set(2, email);
        set(3, themeid);
        set(4, roleid);
    }
}
