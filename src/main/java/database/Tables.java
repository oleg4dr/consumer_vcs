/*
 * This file is generated by jOOQ.
 */
package database;


import database.tables.Forums;
import database.tables.Messages;
import database.tables.Roles;
import database.tables.Themes;
import database.tables.Users;

import javax.annotation.Generated;


/**
 * Convenience access to all tables in Clients
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.3"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Tables {

    /**
     * The table <code>Clients.forums</code>.
     */
    public static final Forums FORUMS = Forums.FORUMS;

    /**
     * The table <code>Clients.messages</code>.
     */
    public static final Messages MESSAGES = Messages.MESSAGES;

    /**
     * The table <code>Clients.roles</code>.
     */
    public static final Roles ROLES = Roles.ROLES;

    /**
     * The table <code>Clients.themes</code>.
     */
    public static final Themes THEMES = Themes.THEMES;

    /**
     * The table <code>Clients.users</code>.
     */
    public static final Users USERS = Users.USERS;
}
