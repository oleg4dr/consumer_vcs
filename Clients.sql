/*
 Navicat Premium Data Transfer

 Source Server         : PostgreSQL
 Source Server Type    : PostgreSQL
 Source Server Version : 120000
 Source Host           : localhost:5432
 Source Catalog        : postgres
 Source Schema         : Clients

 Target Server Type    : PostgreSQL
 Target Server Version : 120000
 File Encoding         : 65001

 Date: 27/02/2020 03:24:50
*/


-- ----------------------------
-- Table structure for forums
-- ----------------------------
DROP TABLE IF EXISTS "Clients"."forums";
CREATE TABLE "Clients"."forums" (
  "forumId" uuid NOT NULL,
  "name" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of forums
-- ----------------------------
INSERT INTO "Clients"."forums" VALUES ('0aa1e14d-361a-4e46-b661-508cb0217e18', 'name of some forum');

-- ----------------------------
-- Table structure for messages
-- ----------------------------
DROP TABLE IF EXISTS "Clients"."messages";
CREATE TABLE "Clients"."messages" (
  "messageId" uuid NOT NULL,
  "userId" uuid NOT NULL,
  "forumId" uuid,
  "body" varchar(255) COLLATE "pg_catalog"."default",
  "subject" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of messages
-- ----------------------------
INSERT INTO "Clients"."messages" VALUES ('81e49489-731d-494a-950e-81cba3ed9cd3', '82e9f457-2815-4dd7-8778-5b5538979859', '0aa1e14d-361a-4e46-b661-508cb0217e18', 'body of some msg', 'subject of some msg');

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS "Clients"."roles";
CREATE TABLE "Clients"."roles" (
  "canEdit" bool NOT NULL,
  "roleId" uuid NOT NULL,
  "canDelete" bool NOT NULL,
  "canPost" bool NOT NULL
)
;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO "Clients"."roles" VALUES ('t', '1e0a6529-b796-41a3-8e2d-476925561f00', 'f', 't');

-- ----------------------------
-- Table structure for themes
-- ----------------------------
DROP TABLE IF EXISTS "Clients"."themes";
CREATE TABLE "Clients"."themes" (
  "themeId" uuid NOT NULL,
  "font" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "language" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "color" varchar(255) COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Records of themes
-- ----------------------------
INSERT INTO "Clients"."themes" VALUES ('a3c0b17a-5fb5-4f4a-9dee-284d0b65d6cc', 'some font', 'English', 'black');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS "Clients"."users";
CREATE TABLE "Clients"."users" (
  "userId" uuid NOT NULL,
  "username" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "email" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "themeId" uuid NOT NULL,
  "roleId" uuid NOT NULL
)
;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO "Clients"."users" VALUES ('82e9f457-2815-4dd7-8778-5b5538979859', 'username', 'user@name.com', 'a3c0b17a-5fb5-4f4a-9dee-284d0b65d6cc', '1e0a6529-b796-41a3-8e2d-476925561f00');

-- ----------------------------
-- Primary Key structure for table forums
-- ----------------------------
ALTER TABLE "Clients"."forums" ADD CONSTRAINT "forums_pkey" PRIMARY KEY ("forumId");

-- ----------------------------
-- Primary Key structure for table messages
-- ----------------------------
ALTER TABLE "Clients"."messages" ADD CONSTRAINT "messages_pkey" PRIMARY KEY ("messageId");

-- ----------------------------
-- Primary Key structure for table roles
-- ----------------------------
ALTER TABLE "Clients"."roles" ADD CONSTRAINT "roles_pkey" PRIMARY KEY ("roleId");

-- ----------------------------
-- Primary Key structure for table themes
-- ----------------------------
ALTER TABLE "Clients"."themes" ADD CONSTRAINT "themes_pkey" PRIMARY KEY ("themeId");

-- ----------------------------
-- Primary Key structure for table users
-- ----------------------------
ALTER TABLE "Clients"."users" ADD CONSTRAINT "users_pkey" PRIMARY KEY ("userId");

-- ----------------------------
-- Foreign Keys structure for table messages
-- ----------------------------
ALTER TABLE "Clients"."messages" ADD CONSTRAINT "FK_message_forum" FOREIGN KEY ("forumId") REFERENCES "Clients"."forums" ("forumId") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "Clients"."messages" ADD CONSTRAINT "FK_message_user" FOREIGN KEY ("userId") REFERENCES "Clients"."users" ("userId") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table users
-- ----------------------------
ALTER TABLE "Clients"."users" ADD CONSTRAINT "fk_role_id" FOREIGN KEY ("roleId") REFERENCES "Clients"."roles" ("roleId") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "Clients"."users" ADD CONSTRAINT "fk_theme_id" FOREIGN KEY ("themeId") REFERENCES "Clients"."themes" ("themeId") ON DELETE NO ACTION ON UPDATE NO ACTION;
